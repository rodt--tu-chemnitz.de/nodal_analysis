import numpy as np
import math

from constants import LAYER_HEIGHT

class Line:
    # basics

    def __init__(self, r0, dr, y=.0):
        self.name = 'TBD'

        self.r0 = np.array(r0)
        self.dr = np.array(dr)
        self.y = y

        self.friends = []

        # if connecting to right or left side of box, making their vertices eligible
        # for being the electrodes
        self.connects_left = False
        self.connects_right = False

    # methods

    def intersection(self, line, use_y=None):
        '''
        Calculates the intersection of the two 2D-Lines.

        a1 t1 + b1 = a2 t2 + b2

        use_y determines if the y-coordinates of the lines play a role in looking for
        an intersection:

        None... y is not considered
        'layer'... only lines in the same layer can intersect (for network generation)
        'tunneling'... only lines in adjacent layers can intersect (for conductance)
        '''
        
        # line params

        a_1 = self.dr
        b_1 = self.r0

        a_2 = line.dr
        b_2 = line.r0


        # print('\n\nCalculating intersection of')
        # print('l_1 = {0} * x + {1}'.format(a_1, b_1))
        # print('l_2 = {0} * x + {1}'.format(a_2, b_2))

        # calculating intersection

        b_delta = b_2 - b_1
        # attention: a has dimension 2, so only z component is returned by cross
        a_cross = np.cross(a_1, a_2)

        t_1 = np.cross(b_delta, a_2) / a_cross
        t_2 = np.cross(b_delta, a_1) / a_cross

        r_intersect = a_1 * t_1 + b_1

        # checking if intersecting
        if (t_1 > .0 and t_1 < 1.) and (t_2 > .0 and t_2 < 1.):
            is_intersecting = True
        else:
            is_intersecting = False
        
        if is_intersecting:
            # lines in same layer can intersect        
            if use_y == 'layer':
                layerdist = abs( self.y - line.y )
                
                if layerdist > 10**(-3):
                    is_intersecting = False
            # lines in adjacent layers can intersect       
            elif use_y == 'tunneling':
                layerdist = abs( self.y - line.y )

                if abs(layerdist - LAYER_HEIGHT) > 10**(-3):
                    is_intersecting = False
            # if y-component doesn't matter, nothing needs to be done
            elif use_y is None:
                pass
            else:
                print('ERROR: invalid option of use_y={0}'.format(use_y))
                raise NotImplementedError

        
        return [is_intersecting, r_intersect]
        
