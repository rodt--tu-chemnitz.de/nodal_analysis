import scipy.constants as sc
import numpy as np

e = sc.elementary_charge
h = sc.Planck
me = sc.electron_mass

# carbon-carbon-distance
A_CC = 1.42 # AA
LAYER_HEIGHT = 3.35 # AA

# conductance quantum
G0 = 2 * e**2 / h # sievert (S)

# current flowing through the network
CURRENT = 1. # A

# conductivities
# all conductivities in G0

'''

# conductance of connections in line
# a GNR with A = 64 AA^2 has a conductance of about 2
CONDUCTANCE_LINE = 2.

# print('CONDUCTANCE_LINE =\t{0:.2f} G0'.format(CONDUCTANCE_LINE))

# tunneling conductance between GNRs

# Area of GNR unit-cell
A = 64 * 10**(-20) # m^2
# distance
d = LAYER_HEIGHT * 10**(-10) # m
# tunneling barrier
lambd = np.array([.5, 1., 1.5]) * e # J

b = np.sqrt(2 * me * lambd)

p1 = h**2 * d / (A * e**2 * b)
p2 = np.exp( 4 * np.pi * d * b / h )
RESISTANCE_TUNNELING = p1 * p2
# devide by G0 so unit is G0
CONDUCTANCE_TUNNELING = 1 / (RESISTANCE_TUNNELING * G0)

# print('p1 =\t{0}'.format(p1))
# print('p2 =\t{0}'.format(p2))
# print('RESISTANCE_TUNNELING =\t{0:.2}'.format(RESISTANCE_TUNNELING))
print('CONDUCTANCE_TUNNELING =\t{0} G0'.format(CONDUCTANCE_TUNNELING))

'''