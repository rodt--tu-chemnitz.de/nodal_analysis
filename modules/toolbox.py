'''
contains various useful functions
'''

import numpy as np
import random

def rotationMatrix(theta):
    cos = np.cos(theta)
    sin = np.sin(theta)

    M = [
        [cos, sin],
        [-sin, cos]
        ]
    
    return np.array(M)

ex = np.array([1., .0])
def randomDirection():
    '''
    2D Unit vector pointing in a random direction.
    '''
    
    theta = random.random() * np.pi * 2.

    e = rotationMatrix(theta) @ ex

    return e